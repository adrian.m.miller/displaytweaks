(
#!/system/bin/sh
# Do NOT assume where your module will be located.
# ALWAYS use $MODDIR if you need to know where this script
# and module is placed.
# This will make sure your module will still work
# if Magisk change its mount point in the future
MODDIR=${0%/*}

# wait till boot completed
while [ "$(getprop sys.boot_completed)" != "1" ]; do
    sleep 10
done

# log file
LogFile="/storage/emulated/0/displaytweaklog.txt"

touch "$LogFile"

# clear log file if its exists
echo "" > "$LogFile"

echo "Applying Display Tweaks..." | tee -a "$LogFile"


#display tweaks
resetprop debug.sf.use_phase_offsets_as_durations 1
resetprop debug.sf.late.sf.duration 10500000
resetprop debug.sf.late.app.duration 16600000
resetprop debug.sf.treat_170m_as_sRGB 1
resetprop debug.sf.earlyGl.app.duration 16600000
resetprop debug.sf.frame_rate_multiple_threshold 120

# wait and apply vsync setting
sleep 10
set start vsync

echo "Applying Display Tweaks...done" | tee -a "$LogFile"

)&


