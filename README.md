### What It Does: ###

See here: https://forum.xda-developers.com/t/magisk-module-make-your-phone-as-smooth-as-ios.4616997/

This is just a quick improved version of that simple module

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/displaytweaks/-/releases)**  
![](https://gitlab.com/adrian.m.miller/displaytweaks/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---

